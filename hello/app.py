import click
from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
    return '<h1>Hello Flask!</h1>'

@app.route('/hi/')
@app.route('/hi/<name>')
def hi(name='xin'):
    return '<h1>Hello, %s!' %name

@app.route('/hello', methods=['GET', 'POST'])
def hello():
    name = request.args.get('name', 'Xin')
    return "<h1>Hello, %s</h1>" %name

# @app.cli.command()
# def hello():
#     click.echo('Hello, Python!')
